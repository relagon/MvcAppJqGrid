﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using MvcAppJqGrid.Models;

namespace MvcAppJqGrid.DBContext
{
    public class TodoContext:DbContext
    {
        public DbSet<TodoList> TodoLists { get; set; }
    }
}